//
//  ITTPlayerViewController.m
//  iTunes-TestApp
//
//  Created by Вячеслав on 01.10.16.
//  Copyright © 2016 Вячеслав. All rights reserved.
//

#import "ITTPlayerViewController.h"
#import "Media+CoreDataClass.h"
#import "ITTSettings.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import <AVFoundation/AVFoundation.h>
#import <POP.h>

#define DegreesToRadians(x) (M_PI * x / 180.0)

@interface ITTPlayerViewController ()
{
    IBOutletCollection(NSLayoutConstraint) NSArray *_portrateConstraints;
    IBOutletCollection(NSLayoutConstraint) NSArray *_landscapeConstraints;
    __weak IBOutlet UILabel *_artistLabel;
    __weak IBOutlet UILabel *_trackLabel;
    __weak IBOutlet UIImageView *_artworkImageView;
    __weak IBOutlet UIActivityIndicatorView *_loadActivityIndicator;
    __weak IBOutlet UIImageView *_playerButtonImageView;
    BOOL play;
    
    AVPlayer * _player;
    AVAudioPlayer *_audioPlayer;
    UIImage * _playImage;
    UIImage * _pauseImage;
    UIImage * _bkgImage;
    
}
@end

@implementation ITTPlayerViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addBackButton];
    [self setupUI];
    [self setupPlayer];
    [self addNotifications];
    
}

- (void) addBackButton
{
    //add back button
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_button"]
                                                             style:UIBarButtonItemStylePlain
                                                            target:self
                                                            action:@selector(backButtonClicked)];
    self.navigationItem.leftBarButtonItem = back;
}


- (void)setupUI
{
    [self setTitle:@"Player"];
    [_artistLabel setText:self.media.artistName];
    [_trackLabel setText:self.media.trackName];
    
    [_loadActivityIndicator startAnimating];
    [_artworkImageView sd_setImageWithURL:[NSURL URLWithString:[self.media artworkHDURL]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [_loadActivityIndicator stopAnimating];
        _artworkImageView.layer.shouldRasterize = YES;
    }];
    
    _playImage = [UIImage imageNamed:@"plbtn_play"];
    _pauseImage = [UIImage imageNamed:@"plbtn_pause"];
    _bkgImage = [UIImage imageNamed:@"plbtn_bkgnd"];
    
    [_playerButtonImageView setImage:_playImage];
}

- (void) setupPlayer
{
    play = false;
    NSError *error =  nil;
    _player = [[AVPlayer alloc] initWithURL:[NSURL URLWithString:[self.media previewURL]]];
    //_audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL URLWithString:[self.media previewURL]] error:&error];
    if (error)
    {
        [self showAlertWithErrorMessage:[error localizedDescription]];
    }
}

- (void)addNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setProperOreintation)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:nil];
}

#pragma mark - Events

- (void)backButtonClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setProperOreintation
{
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    if ((orientation == UIDeviceOrientationLandscapeLeft) || (orientation == UIDeviceOrientationLandscapeRight))
    {
        [self landscapeAnimation:true];
    }
    else
        if (orientation == UIDeviceOrientationPortrait)
        {
            [self landscapeAnimation:false];
        }
    
}

- (void) playerDidReachEnd:(NSNotification *)notification
{
    play = false;
    [self playButtonAnimations];
}

#pragma mark - Animations

- (void)landscapeAnimation:(BOOL) landscape
{
    [NSLayoutConstraint deactivateConstraints:landscape ? _portrateConstraints : _landscapeConstraints];
    [NSLayoutConstraint activateConstraints:landscape ? _landscapeConstraints : _portrateConstraints];
    [self.view layoutSubviews];
}

- (void)playButtonAnimations
{
    POPBasicAnimation * disappearAnimation = [SETTINGSMANAGER disappearAnimation];
    [disappearAnimation setCompletionBlock:^(POPAnimation * animation, BOOL finished) {
        if (finished)
        {
            [_playerButtonImageView setImage: play ? _pauseImage : _playImage];
        }
        POPBasicAnimation * appearAnimation = [SETTINGSMANAGER appearAnimation];
        [_playerButtonImageView pop_addAnimation:appearAnimation forKey:kANIMATION_APPEAR];
    }];
    [_playerButtonImageView pop_addAnimation:disappearAnimation forKey:kANIMATION_DISAPPEAR];
}


#pragma mark - Actions

- (IBAction)playerButtonPressed:(UIButton *)sender {
    play = !play;
    [self playButtonAnimations];
    if (play) {
        [_player play];
    } else {
        [_player pause];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
