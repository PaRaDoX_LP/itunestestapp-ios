//
//  ITTListViewController.m
//  iTunes-TestApp
//
//  Created by Вячеслав on 02.10.16.
//  Copyright © 2016 Вячеслав. All rights reserved.
//

#import "ITTiTunesListViewController.h"
#import "ITTMediaTableViewCell.h"
#import "ITTNetworkClient.h"
#import "ITTMediaTableViewCell.h"
#import "ITTNetworkConstants.h"
#import "ITTPlayerViewController.h"

#import <MBProgressHUD/MBProgressHUD.h>
#import <RestKit/RestKit.h>
@import AVKit;

@interface ITTiTunesListViewController () <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>
{
    __weak IBOutlet UISearchBar *_searchBar;
    __weak IBOutlet UITableView *_tableView;
    __weak IBOutlet UIActivityIndicatorView *_activityIndicator;
}

@property (copy, nonatomic) NSArray<Media *> * mediaArray;

@end

@implementation ITTiTunesListViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupTableView];
    [self setupSearchBar];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_searchBar resignFirstResponder];
}

- (void)setupTableView
{
    [_tableView registerNib:[UINib nibWithNibName:@"ITTMediaTableViewCell" bundle:nil] forCellReuseIdentifier:@"ITTMediaTableViewCell"];
    [_tableView setHidden:true];
}

- (void)setupSearchBar
{
    [_searchBar setBackgroundImage:[UIImage new]];
    [UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]].tintColor = [UIColor lightGrayColor];
    [_searchBar setReturnKeyType:UIReturnKeyDone];
}

#pragma mark - Events

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - <UISearchBarDelegate>

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if ([RKObjectManager sharedManager].operationQueue.operations.count > 0)
    {
        [[RKObjectManager sharedManager] cancelAllObjectRequestOperationsWithMethod:RKRequestMethodGET matchingPathPattern:API_URL_SEARCH];
    }
    [searchBar setShowsCancelButton:YES animated:YES];
    if ([searchText length] >= 5)
    {
        [_activityIndicator startAnimating];
        [NETWORKCLIENT getMediaForTerm:searchText withCompletionBlock:^(NSArray<Media *> *mediaArray, NSError *error) {
            [_activityIndicator stopAnimating];
            if (error)
            {
                [self showAlertWithErrorMessage:[error localizedDescription]];
            }
            else
            {
                self.mediaArray = mediaArray;
                if (mediaArray.count > 0)
                {
                    [_tableView setHidden:false];
                }
                else
                {
                    [_tableView setHidden:true];
                }
                [_tableView reloadData];
            }
        }];
    }
    else
    {
        if (_activityIndicator.animating) { [_activityIndicator stopAnimating]; }
    }
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:NO animated:YES];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    if (_activityIndicator.animating) { [_activityIndicator stopAnimating]; }
    searchBar.text = @"";
    self.mediaArray = nil;
    [searchBar resignFirstResponder];
    [_tableView setHidden:true];
    [_tableView reloadData];
}

#pragma mark - <TableViewDataSource>

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.mediaArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ITTMediaTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ITTMediaTableViewCell" forIndexPath:indexPath];
    [cell setupCellWithMedia:[self.mediaArray objectAtIndex:indexPath.row]];
    return cell;
}

#pragma mark - <UITableViewDelegate>

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ITTPlayerViewController * palyerVC = [ITTPlayerViewController viewControllerFromDefaultNib];
    palyerVC.media = [self.mediaArray objectAtIndex:indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    [self.navigationController pushViewController:palyerVC animated:YES];
}

#pragma mark - <UIScrollViewDelegate>

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if ([_searchBar isFirstResponder])
    {
        [_searchBar resignFirstResponder];
    }
}

@end
