//
//  AppDelegate.m
//  iTunes-TestApp
//
//  Created by Вячеслав on 01.10.16.
//  Copyright © 2016 Вячеслав. All rights reserved.
//

#import "AppDelegate.h"
#import "ITTSettings.h"
#import <MagicalRecord/MagicalRecord.h>

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [SETTINGSMANAGER initializeWithMainController];
    
    return YES;
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [MagicalRecord cleanUp];
}

@end
