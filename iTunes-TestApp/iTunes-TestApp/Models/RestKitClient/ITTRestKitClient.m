//
//  ITTRestKitClient.m
//  iTunes-TestApp
//
//  Created by Вячеслав on 01.10.16.
//  Copyright © 2016 Вячеслав. All rights reserved.
//

#import "ITTRestKitClient.h"
#import "ITTNetworkConstants.h"
#import "Media+CoreDataClass.h"

#import <RestKit/RestKit.h>
#import <CoreData/CoreData.h>
#import <MagicalRecord/MagicalRecord.h>

// Use a class extension to expose access to MagicalRecord's private setter methods
@interface NSManagedObjectContext ()

+ (void)MR_setRootSavingContext:(NSManagedObjectContext *)context;
+ (void)MR_setDefaultContext:(NSManagedObjectContext *)moc;

@end

@interface ITTRestKitClient()
{
    RKEntityMapping *mediaMapping;
}
@end

@implementation ITTRestKitClient

+ (id)sharedInstance {
    static ITTRestKitClient *_instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[ITTRestKitClient alloc] init];
    });
    
    return _instance;
}

- (void)setupRestKit {
    [self setupCoreData];
    
    RKObjectManager *managerRest = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:ITUNES_API_BASE_URL]];
    managerRest.managedObjectStore = [RKManagedObjectStore defaultStore];
    [RKObjectManager setSharedManager:managerRest];
    RKLogConfigureByName("RestKit\retwork", RKLogLevelTrace);
    [AFRKNetworkActivityIndicatorManager sharedManager].enabled = YES;
    [RKMIMETypeSerialization registerClass:[RKNSJSONSerialization class] forMIMEType:@"text/javascript"];
    
    [self setupMapping];
    [self setupResponseDescriptions];
}

- (void)setupCoreData {
    // configure RestKit's core data stack
    NSURL *modelURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"iTunes_TestApp" ofType:@"momd"]];
    NSManagedObjectModel * managedObjectModel = [[[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL] mutableCopy];
    RKManagedObjectStore * managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
    NSString *storePath = [RKApplicationDataDirectory() stringByAppendingPathComponent:@"iTunes_TestApp.sqlite"];
    
    NSError *error = nil;
    
    //setup migrating
    NSPersistentStore *persistentStore = [managedObjectStore addSQLitePersistentStoreAtPath:storePath fromSeedDatabaseAtPath:nil  withConfiguration:nil options:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES} error:&error];
    
    if (!persistentStore) {
        [[NSFileManager defaultManager] removeItemAtPath:storePath error:&error];
        [managedObjectStore addSQLitePersistentStoreAtPath:storePath fromSeedDatabaseAtPath:nil  withConfiguration:nil options:@{NSMigratePersistentStoresAutomaticallyOption:@YES} error:&error];
    }
    
    [managedObjectStore createManagedObjectContexts];
    
    // Configure MagicalRecord to use RestKit's Core Data stack
    [NSPersistentStoreCoordinator MR_setDefaultStoreCoordinator:managedObjectStore.persistentStoreCoordinator];
    [NSManagedObjectContext MR_setRootSavingContext:managedObjectStore.persistentStoreManagedObjectContext];
    [NSManagedObjectContext MR_setDefaultContext:managedObjectStore.mainQueueManagedObjectContext];
}

- (void)setupMapping {
    // Media
    mediaMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([Media class])
                                       inManagedObjectStore:[RKObjectManager sharedManager].managedObjectStore];
    
    mediaMapping.identificationAttributes = @[@"artistID", @"collectionID", @"trackID"];
    [mediaMapping addAttributeMappingsFromDictionary:@{
                                                       @"wrapperType"               : @"wrapperType",
                                                       @"kind"                      : @"kind",
                                                       @"artistId"                  : @"artistID",
                                                       @"collectionId"              : @"collectionID",
                                                       @"trackId"                   : @"trackID",
                                                       @"artistName"                : @"artistName",
                                                       @"collectionName"            : @"collectionName",
                                                       @"trackName"                 : @"trackName",
                                                       @"artistViewUrl"             : @"artistViewURL",
                                                       @"collectionViewUrl"         : @"collectionViewURL",
                                                       @"trackViewUrl"              : @"trackViewURL",
                                                       @"previewUrl"                : @"previewURL",
                                                       @"artworkUrl100"             : @"artworkURL",
                                                       @"collectionPrice"           : @"collectionPrice",
                                                       @"trackPrice"                : @"trackPrice",
                                                       @"releaseDate"               : @"releaseDate",
                                                       @"collectionExplicitness"    : @"collectionExplicitness",
                                                       @"trackExplicitness"         : @"trackExplicitness",
                                                       @"discCount"                 : @"discCount",
                                                       @"discNumber"                : @"discNumber",
                                                       @"trackCount"                : @"trackCount",
                                                       @"trackNumber"               : @"trackNumber",
                                                       @"trackTimeMillis"           : @"trackTimeMillis",
                                                       @"country"                   : @"country",
                                                       @"currency"                  : @"currency",
                                                       @"primaryGenreName"          : @"genre",
                                                       @"isStreamable"              : @"isStreamable"}];
}

- (void)setupResponseDescriptions
{
    //Current Weather
    RKResponseDescriptor * mediaDescr = [RKResponseDescriptor responseDescriptorWithMapping:mediaMapping
                                                                                     method:RKRequestMethodGET
                                                                                pathPattern:API_URL_SEARCH
                                                                                    keyPath:@"results"
                                                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [[RKObjectManager sharedManager] addResponseDescriptor:mediaDescr];
}

@end
