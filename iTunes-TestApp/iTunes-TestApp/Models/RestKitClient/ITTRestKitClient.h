//
//  ITTRestKitClient.h
//  iTunes-TestApp
//
//  Created by Вячеслав on 01.10.16.
//  Copyright © 2016 Вячеслав. All rights reserved.
//

#define RESTKITCLIENT [ITTRestKitClient sharedInstance]

@interface ITTRestKitClient : NSObject

+ (id)sharedInstance;
- (void)setupRestKit;

@end
