//
//  Media+CoreDataProperties.m
//  iTunes-TestApp
//
//  Created by Вячеслав on 01.10.16.
//  Copyright © 2016 Вячеслав. All rights reserved.
//

#import "Media+CoreDataProperties.h"

@implementation Media (CoreDataProperties)

+ (NSFetchRequest<Media *> *)fetchRequest {
    return [[NSFetchRequest alloc] initWithEntityName:@"Media"];
}

@dynamic isStreamable;
@dynamic genre;
@dynamic currency;
@dynamic country;
@dynamic trackTimeMillis;
@dynamic trackNumber;
@dynamic trackCount;
@dynamic discNumber;
@dynamic discCount;
@dynamic trackExplicitness;
@dynamic collectionExplicitness;
@dynamic releaseDate;
@dynamic trackPrice;
@dynamic collectionPrice;
@dynamic artworkURL;
@dynamic previewURL;
@dynamic trackViewURL;
@dynamic collectionViewURL;
@dynamic artistViewURL;
@dynamic trackName;
@dynamic collectionName;
@dynamic artistName;
@dynamic trackID;
@dynamic collectionID;
@dynamic artistID;
@dynamic kind;
@dynamic wrapperType;

@end
