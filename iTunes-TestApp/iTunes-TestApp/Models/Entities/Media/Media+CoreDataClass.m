//
//  Media+CoreDataClass.m
//  iTunes-TestApp
//
//  Created by Вячеслав on 01.10.16.
//  Copyright © 2016 Вячеслав. All rights reserved.
//

#import "Media+CoreDataClass.h"

@implementation Media

- (NSString *)releaseDateString
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"YYYY";
    return [formatter stringFromDate:self.releaseDate];
}

- (NSString *)artworkPNGURL
{
    return [self.artworkURL stringByReplacingOccurrencesOfString:@"jpg" withString:@"png"];
}

- (NSString *)artworkHDURL
{
    return [self.artworkURL stringByReplacingOccurrencesOfString:@"100x100" withString:[NSString stringWithFormat:@"500x500"]];
}

@end
