//
//  Media+CoreDataProperties.h
//  iTunes-TestApp
//
//  Created by Вячеслав on 01.10.16.
//  Copyright © 2016 Вячеслав. All rights reserved.
//

#import "Media+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Media (CoreDataProperties)

+ (NSFetchRequest<Media *> *)fetchRequest;

@property (nonatomic) BOOL isStreamable;
@property (nullable, nonatomic, copy) NSString *genre;
@property (nullable, nonatomic, copy) NSString *currency;
@property (nullable, nonatomic, copy) NSString *country;
@property (nonatomic) int32_t trackTimeMillis;
@property (nonatomic) int16_t trackNumber;
@property (nonatomic) int16_t trackCount;
@property (nonatomic) int16_t discNumber;
@property (nonatomic) int16_t discCount;
@property (nullable, nonatomic, copy) NSString *trackExplicitness;
@property (nullable, nonatomic, copy) NSString *collectionExplicitness;
@property (nullable, nonatomic, copy) NSDate *releaseDate;
@property (nonatomic) float trackPrice;
@property (nonatomic) float collectionPrice;
@property (nullable, nonatomic, copy) NSString *artworkURL;
@property (nullable, nonatomic, copy) NSString *previewURL;
@property (nullable, nonatomic, copy) NSString *trackViewURL;
@property (nullable, nonatomic, copy) NSString *collectionViewURL;
@property (nullable, nonatomic, copy) NSString *artistViewURL;
@property (nullable, nonatomic, copy) NSString *trackName;
@property (nullable, nonatomic, copy) NSString *collectionName;
@property (nullable, nonatomic, copy) NSString *artistName;
@property (nullable, nonatomic, copy) NSString *trackID;
@property (nullable, nonatomic, copy) NSString *collectionID;
@property (nullable, nonatomic, copy) NSString *artistID;
@property (nullable, nonatomic, copy) NSString *kind;
@property (nullable, nonatomic, copy) NSString *wrapperType;

@end

NS_ASSUME_NONNULL_END
