//
//  Media+CoreDataClass.h
//  iTunes-TestApp
//
//  Created by Вячеслав on 01.10.16.
//  Copyright © 2016 Вячеслав. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Media : NSManagedObject

- (NSString *)releaseDateString;
- (NSString *)artworkPNGURL;
- (NSString *)artworkHDURL;

@end

NS_ASSUME_NONNULL_END

#import "Media+CoreDataProperties.h"
