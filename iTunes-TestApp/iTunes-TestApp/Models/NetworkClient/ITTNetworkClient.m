//
//  ITTNetworkClient.m
//  iTunes-TestApp
//
//  Created by Вячеслав on 01.10.16.
//  Copyright © 2016 Вячеслав. All rights reserved.
//

#import "ITTNetworkClient.h"
#import "ITTNetworkConstants.h"
#import "Media+CoreDataClass.h"

#import <RestKit/RestKit.h>

@implementation ITTNetworkClient

#pragma mark - Lifecycle

+ (instancetype)sharedInstance {
    static ITTNetworkClient *_instance = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        _instance = [[ITTNetworkClient alloc] init];
    });
    
    return _instance;
}

- (void) getMediaForTerm:(NSString *) term withCompletionBlock:(ittCompletionBlock)completionBlock {
    
    NSDictionary *parameters = @{@"term" : term};
    
    [[RKObjectManager sharedManager] getObject:nil
                                          path:API_URL_SEARCH
                                    parameters:parameters
                                       success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                           NSLog(@"MEDIA RESULT = %@", [mappingResult array]);
                                           NSArray <Media *>*responseDescription = [mappingResult array];
                                           completionBlock(responseDescription, nil);
                                       } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                           //Check error code for -999 for canceled requests
                                           if (error.code != REST_KIT_CANCELED_OPERATION_ERROR)
                                           {
                                               completionBlock(nil, error);
                                           }
                                       }];
}

@end
