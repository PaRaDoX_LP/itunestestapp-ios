//
//  ITTNetworkClient.h
//  iTunes-TestApp
//
//  Created by Вячеслав on 01.10.16.
//  Copyright © 2016 Вячеслав. All rights reserved.
//

#define NETWORKCLIENT [ITTNetworkClient sharedInstance]

@class Media;

typedef void ( ^ittCompletionBlock) (NSArray<Media *>* mediaArray, NSError *error);

@interface ITTNetworkClient : NSObject

+ (id)sharedInstance;

- (void)getMediaForTerm:(NSString *) term withCompletionBlock:(ittCompletionBlock)completionBlock;

@end
