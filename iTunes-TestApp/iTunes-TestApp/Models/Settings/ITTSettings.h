//
//  ITTSettings.h
//  iTunes-TestApp
//
//  Created by Вячеслав on 01.10.16.
//  Copyright © 2016 Вячеслав. All rights reserved.
//

#import <Foundation/Foundation.h>

@class POPBasicAnimation;

#define SETTINGSMANAGER [ITTSettings sharedManager]

@interface ITTSettings : NSObject

+ (id)sharedManager;

- (void)initializeWithMainController;
- (POPBasicAnimation *)disappearAnimation;
- (POPBasicAnimation* )appearAnimation;

@end
