//
//  ITTSettings.m
//  iTunes-TestApp
//
//  Created by Вячеслав on 01.10.16.
//  Copyright © 2016 Вячеслав. All rights reserved.
//

#import "ITTSettings.h"
#import "ITTiTunesListViewController.h"
#import "ITTRestKitClient.h"
#import <POP.h>

@interface ITTSettings()

@property(nonatomic, strong, readonly) UINavigationController *navigationController;
@property(nonatomic, strong, readonly) UIWindow *keyWindow;

@end

@implementation ITTSettings

+ (id)sharedManager {
    static ITTSettings *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[ITTSettings alloc] init];
    });
    return _sharedManager;
}

- (void) initializeWithMainController
{
    [RESTKITCLIENT setupRestKit];
    //init window
    _keyWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    //init navigation
    _navigationController = [[UINavigationController alloc] initWithNavigationBarClass:nil toolbarClass:nil];
    _keyWindow.rootViewController = _navigationController;
    [_keyWindow makeKeyAndVisible];
    [self setupNavBarAppearenceForNavController];
    
    ITTiTunesListViewController *viewController = [ITTiTunesListViewController viewControllerFromDefaultNib];
    [viewController setTitle:@"iTunes TestApp"];
    [_navigationController pushViewController:viewController animated:YES];
}

#pragma mark - UI Customization

- (void)setupNavBarAppearenceForNavController
{
    [[UINavigationBar appearance] setBarStyle:UIBarStyleDefault];
    [_navigationController.navigationBar setBackgroundImage:[UIImage new]
                                              forBarMetrics:UIBarMetricsDefault];
    _navigationController.navigationBar.shadowImage = [UIImage new];
    _navigationController.navigationBar.translucent = NO;
    [_navigationController.navigationBar setBarTintColor:BLUE_COLOR];
    [_navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [_navigationController.navigationBar setTintColor:[UIColor whiteColor]];
}

#pragma mark - Animations

- (POPBasicAnimation *) disappearAnimation
{
    POPBasicAnimation * viewDisappearScaleAnimation = [POPBasicAnimation animationWithPropertyNamed: kPOPViewScaleXY];
    viewDisappearScaleAnimation.beginTime = CACurrentMediaTime();
    viewDisappearScaleAnimation.duration = ANIMATION_DURATION;
    viewDisappearScaleAnimation.fromValue = [NSValue valueWithCGPoint:ANIMATION_FULL_SCALE];
    viewDisappearScaleAnimation.toValue = [NSValue valueWithCGPoint:ANIMATION_ZERO_SCALE];
    return viewDisappearScaleAnimation;
}

- (POPBasicAnimation* ) appearAnimation
{
    POPBasicAnimation * viewAppearScaleAnimation = [POPBasicAnimation animationWithPropertyNamed: kPOPViewScaleXY];
    viewAppearScaleAnimation.beginTime = CACurrentMediaTime();
    viewAppearScaleAnimation.duration = ANIMATION_DURATION;
    viewAppearScaleAnimation.fromValue = [NSValue valueWithCGPoint:ANIMATION_ZERO_SCALE];
    viewAppearScaleAnimation.toValue = [NSValue valueWithCGPoint:ANIMATION_FULL_SCALE];
    return viewAppearScaleAnimation;
}

@end
