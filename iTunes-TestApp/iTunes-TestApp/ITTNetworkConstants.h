//
//  ITTNetworkConstants.h
//  iTunes-TestApp
//
//  Created by Вячеслав on 01.10.16.
//  Copyright © 2016 Вячеслав. All rights reserved.
//

#ifndef ITTNetworkConstants_h
#define ITTNetworkConstants_h

#define ITUNES_API_BASE_URL     @"https://itunes.apple.com"
#define API_URL_SEARCH          @"/search"


#endif /* ITTNetworkConstants_h */
