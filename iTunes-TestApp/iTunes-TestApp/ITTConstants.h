//
//  ITTConstants.h
//  iTunes-TestApp
//
//  Created by Вячеслав on 01.10.16.
//  Copyright © 2016 Вячеслав. All rights reserved.
//

#ifndef ITTConstants_h
#define ITTConstants_h

#define REST_KIT_CANCELED_OPERATION_ERROR -999

#define ANIMATION_DURATION 0.5
#define ANIMATION_ZERO_SCALE CGPointMake(0.1, 0.1)
#define ANIMATION_FULL_SCALE CGPointMake(1.0, 1.0)

//COLORS
#define BLUE_COLOR [UIColor colorWithRed:0 green:122/255.0 blue:255/255.0 alpha:1.0]

//STRINGS
#define  kANIMATION_APPEAR @"AppearAnimation"
#define  kANIMATION_DISAPPEAR @"DisappearAnimation"

#endif /* ITTConstants_h */
