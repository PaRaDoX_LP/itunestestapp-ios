//
//  ITTMediaTableViewCell.h
//  iTunes-TestApp
//
//  Created by Вячеслав on 01.10.16.
//  Copyright © 2016 Вячеслав. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Media;

@interface ITTMediaTableViewCell : UITableViewCell

- (void)setupCellWithMedia:(Media *) media;

@end
