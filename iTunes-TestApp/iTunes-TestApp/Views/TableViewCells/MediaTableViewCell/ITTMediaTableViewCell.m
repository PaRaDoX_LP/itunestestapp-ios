//
//  ITTMediaTableViewCell.m
//  iTunes-TestApp
//
//  Created by Вячеслав on 01.10.16.
//  Copyright © 2016 Вячеслав. All rights reserved.
//

#import "ITTMediaTableViewCell.h"
#import "Media+CoreDataClass.h"

#import <SDWebImage/UIImageView+WebCache.h>

@interface ITTMediaTableViewCell()
{
    __weak IBOutlet UIImageView *_artworkImageView;
    __weak IBOutlet UILabel *_artistLabel;
    __weak IBOutlet UILabel *_trackLabel;
    __weak IBOutlet UILabel *_genreLabel;
    __weak IBOutlet UILabel *_yearLabel;
    __weak IBOutlet UILabel *_albumLabel;
    __weak IBOutlet UIActivityIndicatorView *_loadActivityIndicator;
    
}

@property (strong, nonatomic) Media * media;

@end

@implementation ITTMediaTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setupCellWithMedia:(Media *) media
{
    self.media = media;
    
    [_artistLabel setText:self.media.artistName];
    [_trackLabel setText:self.media.trackName];
    [_albumLabel setText:self.media.collectionName];
    [_genreLabel setText:self.media.genre];
    [_yearLabel setText:[self.media releaseDateString]];
    
    [_loadActivityIndicator startAnimating];
    [_artworkImageView sd_setImageWithURL:[NSURL URLWithString:[self.media artworkPNGURL]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [_loadActivityIndicator stopAnimating];
        _artworkImageView.layer.shouldRasterize = YES;
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
