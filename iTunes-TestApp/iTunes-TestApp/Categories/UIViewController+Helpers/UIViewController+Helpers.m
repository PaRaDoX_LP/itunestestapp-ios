//
//  UIViewController+Helpers.m
//  iTunes-TestApp
//
//  Created by Вячеслав on 01.10.16.
//  Copyright © 2016 Вячеслав. All rights reserved.
//

#import "UIViewController+Helpers.h"

@implementation UIViewController (Helpers)

#pragma mark - Implementation helpers

+ (instancetype)viewControllerFromDefaultNib {
    id vc = [[[self class] alloc] initWithNibName:NSStringFromClass([self class]) bundle:nil];
    return vc;
}

- (void) showAlertWithErrorMessage:(NSString *)message
{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Error" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
