//
//  UIViewController+Helpers.h
//  iTunes-TestApp
//
//  Created by Вячеслав on 01.10.16.
//  Copyright © 2016 Вячеслав. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Helpers)

+ (instancetype) viewControllerFromDefaultNib;
- (void) showAlertWithErrorMessage:(NSString *)message;

@end
